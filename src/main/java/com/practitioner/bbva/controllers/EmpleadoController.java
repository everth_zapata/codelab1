package com.practitioner.bbva.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.models.Empleado;
import com.practitioner.bbva.models.Venta;
import com.practitioner.bbva.service.IEmpleadoService;
import com.practitioner.bbva.service.IVentaService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/rest/empleados")
public class EmpleadoController {

	@Autowired
	IEmpleadoService empleadoService;
	
	@Autowired
	IVentaService ventaService;

	@GetMapping("/listar")
	public CollectionModel<EntityModel<Empleado>> listar() {

		List<Empleado> lista = empleadoService.findAll();

		return CollectionModel.of(lista.stream()
				.map(emp -> EntityModel.of(emp)
						.add(linkTo(methodOn(this.getClass()).getEmpleado(emp.getId())).withSelfRel()))
				.collect(Collectors.toUnmodifiableList()))
				.add(linkTo(methodOn(this.getClass()).listar()).withSelfRel());

	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getEmpleado(@PathVariable(name = "id") long id) {
		
		Empleado e = empleadoService.getEmpleado(id);
		
		if(e==null) {
			return new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}

		return ResponseEntity.ok(EntityModel.of(e).add(linkTo(methodOn(this.getClass()).getEmpleado(e.getId())).withSelfRel()));

	}
	
	@PostMapping
	public ResponseEntity<?> addEmpleado(@RequestBody Empleado empleado) {
		
		empleadoService.addEmpleado(empleado);
		
		return ResponseEntity
				.created(linkTo(methodOn(this.getClass()).getEmpleado(empleado.getId())).withSelfRel().toUri())
				.body("Empleado creado con éxito");
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateEmpleado(@PathVariable(name = "id") long id, @RequestBody Empleado empleado) {
		Empleado e = empleadoService.getEmpleado(id);
		if(e==null) {
			return  new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}
		empleadoService.updateEmpleado(id, empleado);
		return ResponseEntity.ok().body("Empleado actualizado correctamente");	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteProveedor(@PathVariable(name = "id") long id ){
		Empleado e = empleadoService.getEmpleado(id);
		if(e==null) {
			return  new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}
        this.empleadoService.deleteEmpleado(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
	
	@PatchMapping("/{id}")
	public ResponseEntity<?> updateParcialEmpleado(@PathVariable(name = "id") long id, @RequestBody Empleado empleado) {
		Empleado e = empleadoService.getEmpleado(id);
		if(e==null) {
			return  new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}
		if(empleado.getNombre() != null) {
            if(empleado.getNombre().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setNombre(empleado.getNombre());
        }
        if(empleado.getCargo() != null) {
            if(empleado.getCargo().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setCargo(empleado.getCargo());
        }
        if(empleado.getDocumento() != null) {
            if(empleado.getDocumento().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setDocumento(empleado.getDocumento());
        }
		empleadoService.updateEmpleado(id, e);
		return ResponseEntity.ok().body(e);	
	}
	
	
	@GetMapping("/{id}/ventas")
	public CollectionModel<EntityModel<Venta>> getVentas(@PathVariable(name = "id") long id) {
		
		Empleado e = empleadoService.getEmpleado(id);
		
		if(e==null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontró al Empleado");
		}

		List<Venta> ventas = ventaService.ventasXEmpleado(id);
		return CollectionModel.of(ventas.stream()
				.map( v -> EntityModel.of(v)
						.add( 
							 Arrays.asList(
								linkTo(methodOn(VentaController.class).getVenta(v.getNumeroFactura())).withRel("Venta").withTitle("detalle"),
								linkTo(methodOn(this.getClass()).getEmpleado(v.getIdEmpleado())).withRel("Empleado").withTitle("vendedor")
							 )
							)
					)
				.collect(Collectors.toUnmodifiableList()))
		.add(linkTo(methodOn(this.getClass()).getVentas(id)).withSelfRel());

	}
}
