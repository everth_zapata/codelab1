package com.practitioner.bbva.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.models.Empleado;
import com.practitioner.bbva.models.Producto;
import com.practitioner.bbva.models.Venta;
import com.practitioner.bbva.service.IEmpleadoService;
import com.practitioner.bbva.service.IProductoService;
import com.practitioner.bbva.service.IVentaService;

@RestController
@RequestMapping("/rest/ventas")
public class VentaController {
	
	@Autowired
	IVentaService ventaService;
	
	@Autowired
	IProductoService productoService;
	
	@Autowired
	IEmpleadoService empleadoService;
	
	@GetMapping("/listar")
	public CollectionModel<EntityModel<Venta>> listar() {

		List<Venta> lista = ventaService.findAll();

		return CollectionModel.of(lista.stream()
				.map(fact -> EntityModel.of(fact))
				.collect(Collectors.toUnmodifiableList()))
				.add(linkTo(methodOn(this.getClass()).listar()).withSelfRel());

	}

	
	@GetMapping("/{numeroFactura}")
	public ResponseEntity<?> getVenta(@PathVariable(name = "numeroFactura") String numeroFactura) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok(EntityModel.of(v).add(
				Arrays.asList(
						linkTo(methodOn(this.getClass()).getVenta(v.getNumeroFactura())).withSelfRel(),
						linkTo(methodOn(this.getClass()).getProductos(v.getNumeroFactura())).withRel("Productos").withTitle("Lista de productos de la venta")
				)
				));

	}
	
	
	@GetMapping("/{numeroFactura}/productos")
	public ResponseEntity<?> getProductos(@PathVariable(name = "numeroFactura") String numeroFactura) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		
		

		return ResponseEntity.ok(CollectionModel.of(v.getListaProductos().stream().map(
				p-> EntityModel.of(p).add(linkTo(methodOn(ProductoController.class).getProducto(p.getId())).withSelfRel())
				).collect(Collectors.toUnmodifiableList())));

	}
	
	@PostMapping
	public ResponseEntity<?> addVenta(@RequestBody Venta venta) {
		
		if(venta == null || venta.getIdEmpleado() ==null || venta.getListaProductos()== null || venta.getListaProductos().isEmpty()) {
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		Empleado e = empleadoService.getEmpleado(venta.getIdEmpleado());
		if(e==null) {
			return  new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}
		List<Producto> productos = new ArrayList<Producto>();
		for(Producto p : venta.getListaProductos()) {
			if(p.getId() == null )
			{
				p.setId(productoService.addProducto(p));
				productos.add(p);
			}else {
				Producto tmp = productoService.getProducto(p.getId());
				if(tmp==null) {
					return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
				}else {
					productos.add(tmp);
				}
			}
		}
		if(productos.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		venta.setListaProductos(productos);
		ventaService.addVenta(venta);
		
		return ResponseEntity
				.created(linkTo(methodOn(this.getClass()).getVenta(venta.getNumeroFactura())).withSelfRel().toUri())
				.body("Venta creado con éxito");
		
	}
	
	
}
