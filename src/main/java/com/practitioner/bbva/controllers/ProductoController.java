package com.practitioner.bbva.controllers;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.models.Producto;
import com.practitioner.bbva.service.IProductoService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/rest/productos")
public class ProductoController {
	
	@Autowired
	IProductoService productoService;
	
	
	@GetMapping("/listar")
	public CollectionModel<EntityModel<Producto>> listar() {

		List<Producto> lista = productoService.findAll();

		return CollectionModel.of(lista.stream()
				.map(emp -> EntityModel.of(emp)
						.add(linkTo(methodOn(this.getClass()).getProducto(emp.getId())).withSelfRel()))
				.collect(Collectors.toUnmodifiableList()))
				.add(linkTo(methodOn(this.getClass()).listar()).withSelfRel());

	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getProducto(@PathVariable(name = "id") long id) {
		
		Producto e = productoService.getProducto(id);
		
		if(e==null) {
			return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
		}

		return ResponseEntity.ok(EntityModel.of(e).add(linkTo(methodOn(this.getClass()).getProducto(e.getId())).withSelfRel()));

	}
	
	@PostMapping
	public ResponseEntity<?> addProducto(@RequestBody Producto producto) {
		
		productoService.addProducto(producto);
		
		return ResponseEntity
				.created(linkTo(methodOn(this.getClass()).getProducto(producto.getId())).withSelfRel().toUri())
				.body("Producto creado con éxito");
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateProducto(@PathVariable(name = "id") long id, @RequestBody Producto producto) {
		Producto e = productoService.getProducto(id);
		if(e==null) {
			return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
		}
		productoService.updateProducto(id, producto);
		return ResponseEntity.ok().body("Producto actualizado correctamente");	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteProveedor(@PathVariable(name = "id") long id ){
		Producto e = productoService.getProducto(id);
		if(e==null) {
			return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
		}
        this.productoService.deleteProducto(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
	
	@PatchMapping("/{id}")
	public ResponseEntity<?> updateParcialProducto(@PathVariable(name = "id") long id, @RequestBody Producto producto) {
		Producto e = productoService.getProducto(id);
		if(e==null) {
			return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
		}
		if(producto.getNombre() != null) {
            if(producto.getNombre().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setNombre(producto.getNombre());
        }
        if(producto.getMarca() != null) {
            if(producto.getMarca().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setMarca(producto.getMarca());
        }
        if(producto.getCantidad() != null) {
            if(producto.getCantidad() < 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setCantidad(producto.getCantidad());
        }
        if(producto.getPrecio() != null) {
            if(producto.getCantidad() < 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            e.setPrecio(producto.getPrecio());
        }
		productoService.updateProducto(id, e);
		return ResponseEntity.ok().body(e);	
	}
	

}
