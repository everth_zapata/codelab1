package com.practitioner.bbva.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.practitioner.bbva.models.Venta;

@Service
public class VentaServiceImpl implements IVentaService{

	final ConcurrentHashMap<String, Venta> ventas = new ConcurrentHashMap<String, Venta>();
	
	@Override
	public List<Venta> findAll() {
		// TODO Auto-generated method stub
		return new ArrayList<Venta>(ventas.values());
	}

	@Override
	public String addVenta(Venta venta) {
		venta.initVenta();
		ventas.put(venta.getNumeroFactura(), venta);
		return venta.getNumeroFactura();
	}

	@Override
	public Venta getVenta(String numeroFactura) {
		// TODO Auto-generated method stub
		return ventas.get(numeroFactura);
	}

	@Override
	public List<Venta> ventasXEmpleado(long id) {
		return ventas.values().stream().filter(v -> v.getIdEmpleado() == id).collect(Collectors.toUnmodifiableList());
	}

}
