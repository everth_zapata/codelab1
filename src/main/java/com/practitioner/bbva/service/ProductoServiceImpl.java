package com.practitioner.bbva.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.models.Producto;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	
	final ConcurrentHashMap<Long, Producto> productos = new ConcurrentHashMap<Long, Producto>();
	
	@Override
	public List<Producto> findAll() {
		return new ArrayList<Producto>(productos.values());
	}

	@Override
	public long addProducto(Producto producto) {
		if(producto.getNombre()==null || producto.getPrecio() == null || producto.getPrecio()< 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Es Necesario el Nombre y Precio");
		producto.setId();
		productos.put(producto.getId(), producto);
		return producto.getId();
	}

	@Override
	public Producto getProducto(long id) {
		return productos.get(id);	
	}

	@Override
	public void updateProducto(long id, Producto producto) {
		
		producto.setId(id);
		productos.put(id, producto);
		
	}

	@Override
	public void deleteProducto(long id) {
		productos.remove(id);	
	}
	

}
