package com.practitioner.bbva.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.practitioner.bbva.models.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{

	final ConcurrentHashMap<Long, Empleado> empleados = new ConcurrentHashMap<Long, Empleado>();
	
	@Override
	public List<Empleado> findAll() {
		// TODO Auto-generated method stub
		return new ArrayList<Empleado>(empleados.values());
	}

	@Override
	public long addEmpleado(Empleado empleado) {
		empleado.setId();
		empleados.put(empleado.getId(), empleado);
		return empleado.getId();
	}

	@Override
	public Empleado getEmpleado(long id) {
		// TODO Auto-generated method stub	
		return empleados.get(id);
		
	}

	@Override
	public void updateEmpleado(long id, Empleado empleado) {
		
		empleado.setId(id);
		empleados.put(id, empleado);
		
	}

	@Override
	public void deleteEmpleado(long id) {
	   empleados.remove(id);	
	}


}
