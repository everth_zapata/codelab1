package com.practitioner.bbva.service;

import java.util.List;

import com.practitioner.bbva.models.Empleado;

public interface IEmpleadoService {
	
	public List<Empleado> findAll();
	
	public long addEmpleado(Empleado empleado);

	public Empleado getEmpleado(long id);

	public void updateEmpleado(long id, Empleado empleado);
	
	public void deleteEmpleado(long id);

}
