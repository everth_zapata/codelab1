package com.practitioner.bbva.service;

import java.util.List;

import com.practitioner.bbva.models.Producto;

public interface IProductoService {
	
	public List<Producto> findAll();
	
	public long addProducto(Producto producto);
	
	public Producto getProducto(long id);
	
	public void updateProducto(long id, Producto producto);
	
	public void deleteProducto(long id);
	
}
