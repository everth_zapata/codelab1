package com.practitioner.bbva.service;

import java.util.List;

import com.practitioner.bbva.models.Venta;

public interface IVentaService {
	public List<Venta> findAll();
	public String addVenta(Venta venta);
	public Venta getVenta(String numeroFactura);
	public List<Venta> ventasXEmpleado(long id);
}
