package com.practitioner.bbva;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.practitioner.bbva.models.Empleado;
import com.practitioner.bbva.models.Venta;
import com.practitioner.bbva.models.Producto;
import com.practitioner.bbva.service.IEmpleadoService;
import com.practitioner.bbva.service.IVentaService;
import com.practitioner.bbva.service.IProductoService;

@SpringBootApplication
public class CodeLab1Application implements CommandLineRunner{
	
	@Autowired
	IProductoService productoService;
	
	@Autowired
	IEmpleadoService empleadoService;
	
	@Autowired
	IVentaService ventaService;

	public static void main(String[] args) {
		SpringApplication.run(CodeLab1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Producto producto1 = new Producto();
		producto1.setMarca("Nike");
		producto1.setNombre("Zapatilla");
		producto1.setCantidad(2.0);
		producto1.setPrecio(100.0);
		Producto producto2 = new Producto();
		producto2.setMarca("Addidas");
		producto2.setNombre("Polo");
		producto2.setCantidad(1.0);
		producto2.setPrecio(50.0);
		Producto producto3 = new Producto();
		producto3.setMarca("BookStored");
		producto3.setNombre("Book");
		producto3.setCantidad(1.0);
		producto3.setPrecio(20.0);
		productoService.addProducto(producto1);
		productoService.addProducto(producto2);
		productoService.addProducto(producto3);
		
		Empleado empleado1 = new Empleado();
		empleado1.setNombre("John");
		empleado1.setCargo("Vendedor");
		empleado1.setDocumento("46168744");
		empleadoService.addEmpleado(empleado1);
		
		
		Venta venta1 = new Venta();
		venta1.setIdEmpleado(empleado1.getId());
		venta1.setListaProductos(Arrays.asList(producto1,producto2));
		
		ventaService.addVenta(venta1);

	}

	
}
