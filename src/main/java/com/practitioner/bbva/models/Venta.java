package com.practitioner.bbva.models;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Venta {

	private static final AtomicLong secuenciador = new AtomicLong(0);

	private String numeroFactura;
	private Long idEmpleado;
	private List<Producto> listaProductos;
	private Date fecha;

	public void initVenta() {
		this.numeroFactura = String.format("%05d", secuenciador.incrementAndGet());
		this.fecha = new Date();
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Long getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
