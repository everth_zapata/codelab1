package com.practitioner.bbva.models;

import java.util.concurrent.atomic.AtomicLong;

public class Empleado {
	
	private static final AtomicLong secuenciador = new AtomicLong(0); 
	
	private Long id;
	private String nombre;
	private String documento;
	private String cargo;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setId() {
		this.id = secuenciador.getAndIncrement();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

}
