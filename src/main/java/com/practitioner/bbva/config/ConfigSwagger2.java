package com.practitioner.bbva.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class ConfigSwagger2 {
    @Bean
    public Docket apiDocs() {
        return new Docket(DocumentationType.SWAGGER_2);
    }
}
